using AutocodeTask43;
using NUnit.Framework;

namespace AutoCodeTask43.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var obj = new Class1();
            int expected = 10;
            int actual = obj.DoSomething("Hello");
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void Test2()
        {
            var obj = new Class1();
            int expected = 10;
            int actual = obj.DoSomething("Hello again");
            Assert.AreEqual(expected, actual);

        }
    }
}